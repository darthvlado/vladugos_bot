#!/bin/bash

if [[ !(-z $1) && ($1 = "-r" || $1 = "--reload") ]]
then
	rm -rf cmake
	mkdir cmake
fi

cd cmake
eval "cmake -G \"Unix Makefiles\" .."
make
