#include "onCommonCommands.h"

#include "common.h"

#include <boost/algorithm/string.hpp>
#include <iostream>

using namespace std;

function<void(const TgBot::Message::Ptr&)> onAnyMessage(TgBot::Bot& bot, nlohmann::json& db, mt19937& rng) {
    return [&bot, &db, &rng](const TgBot::Message::Ptr& message) {
        log(message, true);

        bool random_mutes_enabled{
                db["chats"][to_string(message->chat->id)]["random_mutes_enabled"] == nullptr
                ? false
                : db["chats"][to_string(message->chat->id)]["random_mutes_enabled"].get<bool>()
        };

        if (random_mutes_enabled && !is_admin(message, bot)) {
            uniform_real_distribution<double> random(0, 100);
            double rand = random(rng);

            double probability_factor = exp(-1.0 / 8 * get_length(message->text)) / exp(-20.0 / 8);
            double probability =
                    db["chats"][to_string(message->chat->id)]["random_mutes_probability"].get<double>() *
                    probability_factor;
            if (rand <= probability) {
                uniform_int_distribution<int> seconds(
                        db["chats"][to_string(message->chat->id)]["random_mutes_min_seconds"].get<int>(),
                        db["chats"][to_string(message->chat->id)]["random_mutes_max_seconds"].get<int>()
                );
                int sec = seconds(rng);

                if (probability > 100)
                    probability = 100;

                bot.getApi().sendMessage(
                        message->chat->id,
                        get_mention(message) + ", ты заглушен\\_а на " + to_string(sec)
                        + " с. (" + to_string(probability) + "%)",
                        false,
                        0,
                        make_shared<TgBot::GenericReply>(),
                        "Markdown"
                );

                bot.getApi().restrictChatMember(
                        message->chat->id,
                        message->from->id,
                        sec + time(nullptr),
                        false,
                        false,
                        false,
                        false
                );
            }
        }


    };
}

function<void(const TgBot::Message::Ptr&)> onCommand_switch_random_mutes(TgBot::Bot& bot, nlohmann::json& db) {
    return [&bot, &db](const TgBot::Message::Ptr& message) {
        if (!is_bot_admin(message, bot) || !is_admin(message, bot))
            return;
        if (message->chat->type == TgBot::Chat::Type::Private)
            return;

        bool random_mutes_enabled = db["chats"][to_string(message->chat->id)]["random_mutes_enabled"] == nullptr
                                    ? false
                                    : db["chats"][to_string(
                        message->chat->id)]["random_mutes_enabled"].get<bool>();

        if (!random_mutes_enabled) {
            if (db["chats"][to_string(message->chat->id)]["random_mutes_probability"] == nullptr) {
                db["chats"][to_string(message->chat->id)]["random_mutes_probability"] = 1.0;
                db["chats"][to_string(message->chat->id)]["random_mutes_min_seconds"] = 45;
                db["chats"][to_string(message->chat->id)]["random_mutes_max_seconds"] = 120;
            }

            bot.getApi().sendMessage(
                    message->chat->id,
                    "Включены случайные заглушения с вероятностью "
                    + to_string(
                            db["chats"][to_string(message->chat->id)]["random_mutes_probability"].get<double>())
                    + "% от "
                    + to_string(
                            db["chats"][to_string(message->chat->id)]["random_mutes_min_seconds"].get<int>())
                    + " секунд до "
                    + to_string(
                            db["chats"][to_string(message->chat->id)]["random_mutes_max_seconds"].get<int>())
                    + " секунд!"
            );

        } else {
            bot.getApi().sendMessage(
                    message->chat->id,
                    "Случайные заглушения отключены."
            );
        }

        db["chats"][to_string(message->chat->id)]["random_mutes_enabled"] = !random_mutes_enabled;
        update_database(db);
    };
}

function<void(const TgBot::Message::Ptr&)> onCommand_set_random_mutes(TgBot::Bot& bot, nlohmann::json& db) {
    return [&bot, &db](const TgBot::Message::Ptr& message) {
        if (!is_bot_admin(message, bot) || !is_admin(message, bot))
            return;
        if (message->chat->type == TgBot::Chat::Type::Private)
            return;

        cut_command_message(message);

        double random_mutes_probability = 0;
        int random_mutes_min_seconds = 0;
        int random_mutes_max_seconds = 0;

        stringstream ss(message->text);
        ss >> random_mutes_probability;
        ss >> random_mutes_min_seconds;
        ss >> random_mutes_max_seconds;

        if (random_mutes_probability <= 0 || random_mutes_probability > 100
            || random_mutes_min_seconds < 40 || random_mutes_min_seconds > random_mutes_max_seconds
                ) {
            bot.getApi().sendMessage(
                    message->chat->id,
                    "Вы должны ввести параметры в следующем порядке: вероятность, минимальное количество секунд "
                    + string("(не менее 40), максимальное количество секунд. Попробуйте снова.")
            );
            return;
        }

        db["chats"][to_string(message->chat->id)]["random_mutes_probability"] = random_mutes_probability;
        db["chats"][to_string(message->chat->id)]["random_mutes_min_seconds"] = random_mutes_min_seconds;
        db["chats"][to_string(message->chat->id)]["random_mutes_max_seconds"] = random_mutes_max_seconds;

        bot.getApi().sendMessage(
                message->chat->id,
                "Установлены параметры случайного заглушения: вероятность "
                + to_string(
                        db["chats"][to_string(message->chat->id)]["random_mutes_probability"].get<double>())
                + "% от "
                + to_string(
                        db["chats"][to_string(message->chat->id)]["random_mutes_min_seconds"].get<int>())
                + " секунд до "
                + to_string(
                        db["chats"][to_string(message->chat->id)]["random_mutes_max_seconds"].get<int>())
                + " секунд."
        );

        update_database(db);
    };
}

function<void(const TgBot::Message::Ptr&)> onCommand_start(TgBot::Bot& bot) {
    return [&bot](const TgBot::Message::Ptr& message) {
        bot.getApi().sendMessage(
                message->chat->id,
                "Я работаю!"
        );
    };
}

function<void(const TgBot::Message::Ptr&)> onCommand_help(TgBot::Bot& bot) {
    return [&bot](const TgBot::Message::Ptr& message) {
        bot.getApi().sendMessage(
                message->chat->id,
                R"(
/start - проверка, не упал ли бот
/help_latex - справка по LaTeX
/help_dice - справка по дайсовой нотации
/or - [a, b...] выбор случайного варианта
/roll - [N] случайное целое число от 0 до N
/dice - [PATTERN] бросить кости
/truth - проверка истинности фразы
/me - [MSG] сказать от третьего лица
/latex - [CODE] LaTeX
/latexeq - [CODE] LaTeX с окружением equation
/s/PATTERN/REPLACEMENT - sed (замена)

Для админов:
/kick - пнуть пользователя из чата
/ban - забанить
/mute - [N] залушить на N минут
/unmute - снять заглушение
/set_welcome - [MSG] настроить приветствие
/switch_welcome - вкл/выкл приветствие
/approve - подтвердить
/set_random_mutes - настроить случайные заглушения
/switch_random_mutes - вкл/выкл случайные заглушения

Только в личных сообщениях:
/switch_anon_chat - вкл/выкл анонимный чат
)"
        );
    };
}

function<void(const TgBot::Message::Ptr&)> onCommand_me(TgBot::Bot& bot) {
    return [&bot](const TgBot::Message::Ptr& message) {
        if (is_bot_admin(message, bot)) {
            bot.getApi().deleteMessage(
                    message->chat->id,
                    message->messageId
            );
        }
        string mention = get_mention(message);
        boost::replace_all(message->text, "/me", mention);
        bot.getApi().sendMessage(
                message->chat->id,
                message->text,
                false,
                0,
                make_shared<TgBot::GenericReply>(),
                "Markdown"
        );
    };
}

function<void(const TgBot::Message::Ptr&)> onCommand_truth(TgBot::Bot& bot) {
    return [&bot](const TgBot::Message::Ptr& message) {
        if (!is_reply(message))
            return;

        int k = 0;
        for (char i : message->replyToMessage->text) {
            k += int(i);
        }
        k = mod(k, 101);

        if (is_bot_admin(message, bot)) {
            bot.getApi().deleteMessage(
                    message->chat->id,
                    message->messageId
            );
        }
        bot.getApi().sendMessage(
                message->chat->id,
                "Вероятность этого: " + to_string(k) + "%. " + get_mention(message) + ", убедился?",
                false,
                message->replyToMessage->messageId,
                make_shared<TgBot::GenericReply>(),
                "Markdown"
        );
    };
}

function<void(const TgBot::Message::Ptr&)> onCommand_switch_anon_chat(TgBot::Bot& bot, nlohmann::json& db) {
    return [&bot, &db](const TgBot::Message::Ptr& message) {
        if (message->chat->type != TgBot::Chat::Type::Private)
            return;

        bool chat_enabled = db["chats"][to_string(message->chat->id)]["chat_enabled"] == nullptr
                            ? false
                            : db["chats"][to_string(message->chat->id)]["chat_enabled"].get<bool>();

        vector<int32_t> anons = db["anons"] == nullptr
                                ? vector<int32_t>()
                                : db["anons"].get<vector<int32_t>>();

        if (!chat_enabled) {
            anons.push_back(message->from->id);
            bot.getApi().sendMessage(
                    message->chat->id,
                    R"(
Анонимный чат включён!

Имейте ввиду, что сообщения с командами боту отправлены в чат не будут.
Реплаи не работают и работать не будут.
Работает отправка изображений, GIF, видео, стикеров, документов.
Отправка войсов пока что не работает :(

Для отключения чата повторно используйте /switch_anon_chat
)"
            );
        } else {
            if (auto where = find(anons.begin(), anons.end(), message->from->id); where != anons.end()) {
                anons.erase(where);
            }
            bot.getApi().sendMessage(
                    message->chat->id,
                    "Анонимный чат выключен. Чтобы вернуться обратно, используйте /switch_anon_chat"
            );
        }

        db["anons"] = anons;
        db["chats"][to_string(message->chat->id)]["chat_enabled"] = !chat_enabled;
        update_database(db);
    };
}
