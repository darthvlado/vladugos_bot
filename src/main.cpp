#include "exec.h"
#include "common.h"
#include "onUnknownCommand.h"
#include "onNonCommandMessage.h"
#include "onCommonCommands.h"
#include "onLatexCommands.h"
#include "onRollCommands.h"
#include "onAdminCommands.h"
#include "onWelcomeCommands.h"

#include "json.hpp"
#include <tgbot/tgbot.h>
#include <iostream>

using namespace std;

int main() {
    ifstream db_file("database.json");
    nlohmann::json db;
    db_file >> db;
    db_file.close();

    TgBot::Bot bot(db["token"].get<string>());

    random_device rd;
    mt19937 rng(rd());

    // Common
    bot.getEvents().onAnyMessage(onAnyMessage(bot, db, rng));
    bot.getEvents().onUnknownCommand(onUnknownCommand(bot));
    bot.getEvents().onNonCommandMessage(onNonCommandMessage(bot, db));
    bot.getEvents().onCommand("start", onCommand_start(bot));
    bot.getEvents().onCommand("help", onCommand_help(bot));
    bot.getEvents().onCommand("switch_anon_chat", onCommand_switch_anon_chat(bot, db));
    bot.getEvents().onCommand("switch_random_mutes", onCommand_switch_random_mutes(bot, db));
    bot.getEvents().onCommand("set_random_mutes", onCommand_set_random_mutes(bot, db));
    bot.getEvents().onCommand("me", onCommand_me(bot));
    bot.getEvents().onCommand("truth", onCommand_truth(bot));

    // LaTeX
    bot.getEvents().onCommand("help_latex", onCommand_help_latex(bot));
    bot.getEvents().onCommand("latex", onCommand_latex(bot));
    bot.getEvents().onCommand("latexeq", onCommand_latexeq(bot));

    // Roll
    bot.getEvents().onCommand("help_dice", onCommand_help_dice(bot));
    bot.getEvents().onCommand("dice", onCommand_dice(bot));
    bot.getEvents().onCommand("roll", onCommand_roll(bot, rng));
    bot.getEvents().onCommand("or", onCommand_or(bot, rng));

    // Admin
    bot.getEvents().onCommand("kick", onCommand_kick(bot));
    bot.getEvents().onCommand("ban", onCommand_ban(bot));
    bot.getEvents().onCommand("mute", onCommand_mute(bot));
    bot.getEvents().onCommand("unmute", onCommand_unmute(bot));
    bot.getEvents().onCommand("approve", onCommand_approve(bot, db));

    // Welcome
    bot.getEvents().onCommand("set_welcome", onCommand_set_welcome(bot, db));
    bot.getEvents().onCommand("switch_welcome", onCommand_switch_welcome(bot, db));

    cout << "bot username: " + bot.getApi().getMe()->username << endl;
    bot.getApi().deleteWebhook();
    TgBot::TgLongPoll longPoll(bot);

    while (true) {
        try {
            longPoll.start();
        } catch (exception& e) {
            auto t = time(nullptr);
            stringstream ss;
            ss << put_time(gmtime(&t), "%c %Z") << '\t' << string(e.what()) << endl;
            cout << ss.str();
            ofstream logFile("log/log.txt", ios::app | ios::out);
            logFile << ss.str();
        }
    }

    return 0;
}
