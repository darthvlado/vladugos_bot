#pragma once

#include <tgbot/Bot.h>
#include <tgbot/types/Message.h>
#include <functional>

std::function<void(const TgBot::Message::Ptr&)> onUnknownCommand(TgBot::Bot& bot);
