#pragma once

#include "json.hpp"
#include <tgbot/Bot.h>
#include <tgbot/types/Message.h>
#include <functional>
#include <random>

std::function<void(const TgBot::Message::Ptr&)> onAnyMessage(TgBot::Bot& bot, nlohmann::json& db, std::mt19937& rng);

std::function<void(const TgBot::Message::Ptr&)> onCommand_switch_random_mutes(TgBot::Bot& bot, nlohmann::json& db);

std::function<void(const TgBot::Message::Ptr&)> onCommand_set_random_mutes(TgBot::Bot& bot, nlohmann::json& db);

std::function<void(const TgBot::Message::Ptr&)> onCommand_start(TgBot::Bot& bot);

std::function<void(const TgBot::Message::Ptr&)> onCommand_help(TgBot::Bot& bot);

std::function<void(const TgBot::Message::Ptr&)> onCommand_me(TgBot::Bot& bot);

std::function<void(const TgBot::Message::Ptr&)> onCommand_truth(TgBot::Bot& bot);

std::function<void(const TgBot::Message::Ptr&)> onCommand_switch_anon_chat(TgBot::Bot& bot, nlohmann::json& db);
