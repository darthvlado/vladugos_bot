#include "onNonCommandMessage.h"

#include "common.h"

#include <thread>
#include <iostream>

using namespace std;

function<void(const TgBot::Message::Ptr&)> onNonCommandMessage(TgBot::Bot& bot, nlohmann::json& db) {
    return [&bot, &db](const TgBot::Message::Ptr& message) {
        if (!message->newChatMembers.empty()) {

            if (message->newChatMember->isBot) {
                bot.getApi().sendMessage(
                        message->chat->id,
                        "БОТ! БОТ! --> @" + message->newChatMember->username + " <-- это бот!",
                        false,
                        message->messageId
                );
                bot.getApi().sendMessage(
                        message->chat->id, "Дадим Скайнету бой! Бот, бот, бот! Здесь появился бот!"
                );
                return;
            }

            if (message->newChatMember->id == 299680495 && message->chat->id == -1001231540042) {
                int count = db["chats"]["-1001231540042"]["Aysel_leave_count"].get<int>();
                ++count;
                bot.getApi().sendMessage(
                        message->chat->id,
                        "ВНИМАНИЕ, ВНИМАНИЕ, ВНИМАНИЕ! Она здесь! Айсель снова в этом чате уже как минимум " +
                        to_string(count) + " раз_а!",
                        false,
                        message->messageId
                );
                db["chats"]["-1001231540042"]["Aysel_leave_count"] = count;
                update_database(db);
            }

            bool state = db["chats"][to_string(message->chat->id)]["welcome_enabled"] == nullptr
                         ? false
                         : db["chats"][to_string(message->chat->id)]["welcome_enabled"].get<bool>();

            if (!state) {
                return;
            }

            vector <int32_t> members = db["chats"][to_string(message->chat->id)]["known_members"] == nullptr
                                       ? vector<int32_t>()
                                       : db["chats"][to_string(message->chat->id)]["known_members"]
                                       .get<vector<int32_t>>();

            if (find(members.begin(), members.end(), message->newChatMember->id) != members.end()) {
                return;
            }

            auto auto_kick = [&bot, &db](int64_t chat_id, int32_t user_id) {
                for (int t = 0; t < 300; ++t) {
                    this_thread::sleep_for(chrono::seconds(1));
                    auto members = db["chats"][to_string(chat_id)]["known_members"] == nullptr
                                               ? vector<int32_t>()
                                               : db["chats"][to_string(chat_id)]["known_members"]
                                                       .get<vector<int32_t>>();
                    if (find(members.begin(), members.end(), user_id) != members.end()) {
                        return;
                    }
                }

                auto member = bot.getApi().getChatMember(chat_id, user_id);
                cout << member->status << endl;
                if(member != nullptr && member->status != "kicked") {
                    bot.getApi().kickChatMember(
                            chat_id,
                            user_id,
                            0
                    );
                    bot.getApi().unbanChatMember(
                            chat_id,
                            user_id
                    );
                }
            };

            thread kicker(auto_kick, message->chat->id, message->newChatMember->id);
            kicker.detach();

            bot.getApi().sendMessage(
                    message->chat->id,
                    db["chats"][to_string(message->chat->id)]["welcome_message"].get<string>(),
                    false,
                    message->messageId
            );
        }

        bool anon_chat_enabled = db["chats"][to_string(message->chat->id)]["chat_enabled"] == nullptr
                                 ? false
                                 : db["chats"][to_string(message->chat->id)]["chat_enabled"].get<bool>();

        if (anon_chat_enabled && message->chat->type == TgBot::Chat::Type::Private) {
            auto anons = db["anons"].get < vector < int32_t >> ();

            for (auto& anon : anons) {
                if (anon != message->from->id) {
                    if (message->voice != nullptr) {
                        bot.getApi().sendVoice(
                                anon,
                                message->voice->file_id,
                                message->caption,
                                message->voice->duration
                        );
                    }
                    if (!message->photo.empty()) {
                        bot.getApi().sendPhoto(
                                anon,
                                message->photo[0]->fileId,
                                message->caption
                        );
                    }
                    if (message->audio != nullptr) {
                        bot.getApi().sendAudio(
                                anon,
                                message->audio->fileId,
                                message->caption
                        );
                    }
                    if (message->sticker != nullptr) {
                        bot.getApi().sendSticker(
                                anon,
                                message->sticker->fileId
                        );
                    }
                    if (message->document != nullptr) {
                        bot.getApi().sendDocument(
                                anon,
                                message->document->fileId
                        );
                    }
                    if (message->video != nullptr) {
                        bot.getApi().sendVideo(
                                anon,
                                message->video->fileId
                        );
                    }
                    if (!message->text.empty()) {
                        bot.getApi().sendMessage(
                                anon,
                                message->text
                        );
                    }
                }
            }

            log(message, true, true);
        }
    };
}

