#pragma once

#include "json.hpp"
#include <tgbot/Bot.h>
#include <tgbot/types/Message.h>

#include <codecvt>

void log(const TgBot::Message::Ptr& message, bool enable_log_file, bool is_anon_chat = false);

int mod(int divident, int divisor);

bool is_admin(const TgBot::Message::Ptr& message, TgBot::Bot& bot);

bool is_bot_admin(const TgBot::Message::Ptr& message, TgBot::Bot& bot);

bool is_this_bot(const TgBot::Message::Ptr& message, TgBot::Bot& bot);

bool is_reply(const TgBot::Message::Ptr& message);

void cut_command_message(const TgBot::Message::Ptr& message);

std::string get_any_name(const TgBot::Message::Ptr& message);

std::string get_mention(const TgBot::Message::Ptr& message, std::string mention = "");

std::size_t get_length(std::string& str);

void update_database(nlohmann::json& db);
