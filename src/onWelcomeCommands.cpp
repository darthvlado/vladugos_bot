#include "onWelcomeCommands.h"

#include "common.h"

#include <string>

using namespace std;

function<void(const TgBot::Message::Ptr&)> onCommand_set_welcome(TgBot::Bot& bot, nlohmann::json& db) {
    return [&bot, &db](const TgBot::Message::Ptr& message) {
        if (!is_bot_admin(message, bot) || !is_admin(message, bot))
            return;
        if (message->chat->type == TgBot::Chat::Type::Private)
            return;

        cut_command_message(message);

        db["chats"][to_string(message->chat->id)]["welcome_message"] = message->text;
        update_database(db);

        bot.getApi().sendMessage(
                message->chat->id,
                "Установлено приветствие: \"" + message->text + '\"'
        );
    };
}

function<void(const TgBot::Message::Ptr&)> onCommand_switch_welcome(TgBot::Bot& bot, nlohmann::json& db) {
    return [&bot, &db](const TgBot::Message::Ptr& message) {
        if (!is_bot_admin(message, bot) || !is_admin(message, bot))
            return;
        if (message->chat->type == TgBot::Chat::Type::Private)
            return;

        bool state = db["chats"][to_string(message->chat->id)]["welcome_enabled"] == nullptr
                     ? false
                     : db["chats"][to_string(message->chat->id)]["welcome_enabled"].get<bool>();

        if (state) {
            db["chats"][to_string(message->chat->id)]["welcome_enabled"] = false;
            bot.getApi().sendMessage(
                    message->chat->id,
                    "Приветствие выключено"
            );
        } else {
            db["chats"][to_string(message->chat->id)]["welcome_enabled"] = true;

            string str;
            if (db["chats"][to_string(message->chat->id)]["welcome_message"] == nullptr) {
                str = "Привет!";
                db["chats"][to_string(message->chat->id)]["welcome_message"] = str;

            } else {
                str = db["chats"][to_string(message->chat->id)]["welcome_message"].get<string>();
            }

            bot.getApi().sendMessage(
                    message->chat->id,
                    "Приветствие включено, текущее приветствие: \"" + str + "\""
            );
        }

        update_database(db);
    };
}