#include "onRollCommands.h"

#include "exec.h"
#include "common.h"


#include <boost/algorithm/string/trim.hpp>
#include <string>
#include <filesystem>
#include <vector>
#include <future>
#include <iostream>

using namespace std;

function<void(const TgBot::Message::Ptr&)> onCommand_help_dice(TgBot::Bot& bot) {
    return [&bot](const TgBot::Message::Ptr& message) {
        bot.getApi().sendMessage(
                message->chat->id,
                R"(
СПРАВКА ПО ДАЙСОВОЙ НОТАЦИИ:

Использование:
• Вызов без аргументов - бросок 1 кубика.
• Считаются все основные арифметические операции.
• Xdn — дает список X бросков куба с n гранями.
• Если над Xdn нет арифметических операций, то вернётся список отдельных бросков, иначе считается общая сумма.

Дополнительные инструменты:
• d% — короткое d100.
• un — кидает от -n до n.
• df — 0, 1, -1.
• e, f — неравенства >=, <. Выдает 0 или 1.
• 1w6 — дикий куб.
• s, t, | — сортировка списка, сумма списка, объединение списков.
XdY^,m,vn —  n максимальных/средних/минимальных кубов.

Сетапы:
• 4u1 — бросок Фейта .
• d20e<сложность> — определяет успех .
• 20d6s — сетап для Фиаско.

Все возможности:
• https://github.com/borntyping/python-dice
)",
                true
        );
    };
}

function<void(const TgBot::Message::Ptr&)> onCommand_dice(TgBot::Bot& bot) {
    return [&bot](const TgBot::Message::Ptr& message) {
        cut_command_message(message);
        if (message->text.empty()) {
            message->text = "d6";
        }

        string bin = filesystem::exists("/bin/dice")
                          ? "/bin/dice"
                          : "/usr/local/bin/dice";

        volatile pid_t pid = 0;
        auto p_pid = &pid;

        future<string> future = async(launch::async, [&bin, &message, &p_pid]() {
            return exec(vector<string>{bin, message->text}, true, p_pid);
        });

        future_status status = future.wait_for(chrono::seconds(1));
        if (status == future_status::timeout) {
            kill(pid, SIGKILL);
            bot.getApi().sendMessage(
                    message->chat->id,
                    "Превышено время ожидания"
            );
            return;
        }
        string result = future.get();
        bot.getApi().sendMessage(
                message->chat->id,
                result,
                false,
                message->messageId
        );
    };
}

function<void(const TgBot::Message::Ptr&)> onCommand_roll(TgBot::Bot& bot, mt19937& rng) {
    return [&bot, &rng](const TgBot::Message::Ptr& message) {
        cut_command_message(message);

        int max;
        try {
            max = stoi(message->text);
        } catch (const std::invalid_argument& ia) {
            return;
        }

        if (max < 0)
            return;

        uniform_int_distribution<int> uni(0, max);

        bot.getApi().sendMessage(
                message->chat->id,
                to_string(uni(rng)),
                false,
                message->messageId
        );
    };
}

function<void(const TgBot::Message::Ptr&)> onCommand_or(TgBot::Bot& bot, mt19937& rng) {
    return [&bot, &rng](const TgBot::Message::Ptr& message) {
        cut_command_message(message);

        vector<string> variants;

        stringstream ss(message->text);
        string value;
        while (getline(ss, value, ',')) {
            boost::algorithm::trim(value);
            variants.push_back(value);
        }

        if(variants.empty())
            return;

        uniform_int_distribution<int> uni(0, variants.size()-1);

        bot.getApi().sendMessage(
                message->chat->id,
                variants[uni(rng)],
                false,
                message->messageId
        );



    };
}
