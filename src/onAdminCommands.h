#pragma once

#include "json.hpp"
#include <tgbot/Bot.h>
#include <tgbot/types/Message.h>
#include <functional>

std::function<void(const TgBot::Message::Ptr&)> onCommand_kick(TgBot::Bot& bot);

std::function<void(const TgBot::Message::Ptr&)> onCommand_ban(TgBot::Bot& bot);

std::function<void(const TgBot::Message::Ptr&)> onCommand_mute(TgBot::Bot& bot);

std::function<void(const TgBot::Message::Ptr&)> onCommand_unmute(TgBot::Bot& bot);

std::function<void(const TgBot::Message::Ptr&)> onCommand_approve(TgBot::Bot& bot, nlohmann::json& db);
