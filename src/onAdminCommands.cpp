#include "onAdminCommands.h"

#include "common.h"

#include <string>

using namespace std;

function<void(const TgBot::Message::Ptr&)> onCommand_kick(TgBot::Bot& bot) {
    return [&bot](const TgBot::Message::Ptr& message) {
        if (!is_reply(message)
            || is_this_bot(message->replyToMessage, bot)
            || !is_admin(message, bot)
            || is_admin(message->replyToMessage, bot)
            || !is_bot_admin(message, bot))
            return;

        bot.getApi().sendMessage(
                message->chat->id,
                get_mention(message->replyToMessage) + " отправлен\\_а пинком из чата восвояси! Но может вернуться.",
                false,
                0,
                make_shared<TgBot::GenericReply>(),
                "Markdown"
        );
        bot.getApi().kickChatMember(
                message->chat->id,
                message->replyToMessage->from->id,
                0
        );
        bot.getApi().unbanChatMember(
                message->chat->id,
                message->replyToMessage->from->id
        );
    };
}

function<void(const TgBot::Message::Ptr&)> onCommand_ban(TgBot::Bot& bot) {
    return [&bot](const TgBot::Message::Ptr& message) {
        if (!is_reply(message)
            || is_this_bot(message->replyToMessage, bot)
            || !is_admin(message, bot)
            || is_admin(message->replyToMessage, bot)
            || !is_bot_admin(message, bot))
            return;

        bot.getApi().sendMessage(
                message->chat->id,
                get_mention(message->replyToMessage) + " отправлен\\_а в бан.",
                false,
                0,
                make_shared<TgBot::GenericReply>(),
                "Markdown"
        );
        bot.getApi().kickChatMember(
                message->chat->id,
                message->replyToMessage->from->id,
                0
        );
    };
}

function<void(const TgBot::Message::Ptr&)> onCommand_mute(TgBot::Bot& bot) {
    return [&bot](const TgBot::Message::Ptr& message) {
        if (!is_reply(message)
            || is_this_bot(message->replyToMessage, bot)
            || !is_admin(message, bot)
            || is_admin(message->replyToMessage, bot)
            || !is_bot_admin(message, bot))
            return;

        cut_command_message(message);
        int minutes = message->text.empty()
                      ? 5
                      : stoi(message->text);

        if (minutes <= 0)
            return;

        bot.getApi().sendMessage(
                message->chat->id,
                get_mention(message->replyToMessage) + " помолчит " + to_string(minutes) + " мин.",
                false,
                0,
                make_shared<TgBot::GenericReply>(),
                "Markdown"
        );

        bot.getApi().restrictChatMember(
                message->chat->id,
                message->replyToMessage->from->id,
                60 * minutes + time(nullptr),
                false,
                false,
                false,
                false
        );
    };
}

function<void(const TgBot::Message::Ptr&)> onCommand_unmute(TgBot::Bot& bot) {
    return [&bot](const TgBot::Message::Ptr& message) {
        if (!is_reply(message)
            || is_this_bot(message->replyToMessage, bot)
            || !is_admin(message, bot)
            || is_admin(message->replyToMessage, bot)
            || !is_bot_admin(message, bot))
            return;

        bot.getApi().restrictChatMember(
                message->chat->id,
                message->replyToMessage->from->id,
                0,
                true,
                true,
                true,
                true
        );

        if (is_bot_admin(message, bot)) {
            bot.getApi().deleteMessage(
                    message->chat->id,
                    message->messageId
            );
        }
    };
}

function<void(const TgBot::Message::Ptr&)> onCommand_approve(TgBot::Bot& bot, nlohmann::json& db) {
    return [&bot, &db](const TgBot::Message::Ptr& message) {
        if (!is_reply(message)
            || is_this_bot(message->replyToMessage, bot)
            || !is_admin(message, bot)
            || is_admin(message->replyToMessage, bot)
            || !is_bot_admin(message, bot))
            return;

        vector <int32_t> members = db["chats"][to_string(message->chat->id)]["known_members"] == nullptr
                                   ? vector<int32_t>()
                                   : db["chats"][to_string(message->chat->id)]["known_members"]
                                           .get<vector<int32_t>>();
        members.push_back(message->replyToMessage->from->id);
        db["chats"][to_string(message->chat->id)]["known_members"] = members;
        update_database(db);

        bot.getApi().sendMessage(
                message->chat->id,
                get_mention(message->replyToMessage) + " допущен\\_а. Приветствуем!",
                false,
                0,
                make_shared<TgBot::GenericReply>(),
                "Markdown"
        );

    };
}
