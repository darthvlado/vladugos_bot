#include "onUnknownCommand.h"

#include "exec.h"
#include "common.h"

#include <string>
#include <filesystem>
#include <future>

using namespace std;

function<void(const TgBot::Message::Ptr&)> onUnknownCommand(TgBot::Bot& bot) {
    return [&bot](const TgBot::Message::Ptr& message) {
        if (message->replyToMessage != nullptr && StringTools::startsWith(message->text, "/s")) {
            vector<string> patterns;
            stringstream ss(message->text);
            string to;

            while (getline(ss, to, '\n')) {
                string pattern = "s" + to.erase(0, 2) + "/g";
                patterns.push_back(pattern);
            }
            string result = message->replyToMessage->text;

            for (const auto& pattern : patterns) {
                auto filename =
                        to_string(chrono::system_clock::now().time_since_epoch().count()) + "-sed.txt";
                auto path = filesystem::path(filesystem::temp_directory_path().string() + '/' + filename);
                ofstream file(path, ios::trunc | ios::out);
                file << result;
                file.close();

                volatile pid_t pid = 0;
                auto p_pid = &pid;

                future<string> future = async(launch::async, [&pattern, &path, &p_pid]() {
                    return exec(vector<string>{"/bin/sed", "--sandbox", "-e", pattern, path}, true, p_pid);

                });

                future_status status = future.wait_for(chrono::seconds(1));
                if (status == future_status::timeout) {
                    kill(pid, SIGKILL);
                    bot.getApi().sendMessage(
                            message->chat->id,
                            "Превышено время ожидания"
                    );
                    return;
                }

                result = future.get();
                filesystem::remove(path);
            }

            if (is_bot_admin(message, bot)) {
                bot.getApi().deleteMessage(
                        message->chat->id,
                        message->messageId
                );
            }

            bot.getApi().sendMessage(
                    message->chat->id,
                    result,
                    false,
                    message->replyToMessage->messageId
            );
        }
    };
}