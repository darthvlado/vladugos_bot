#ifndef ONNONCOMMANDMESSAGE_H
#define ONNONCOMMANDMESSAGE_H


#include "json.hpp"
#include <tgbot/Bot.h>
#include <tgbot/types/Message.h>
#include <functional>
#include <random>

std::function<void(const TgBot::Message::Ptr&)> onNonCommandMessage(
        TgBot::Bot& bot, nlohmann::json& db);

#endif