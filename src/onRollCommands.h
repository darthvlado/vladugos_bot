#pragma once

#include "json.hpp"
#include <tgbot/Bot.h>
#include <tgbot/types/Message.h>
#include <functional>
#include <random>

std::function<void(const TgBot::Message::Ptr&)> onCommand_help_dice(TgBot::Bot& bot);

std::function<void(const TgBot::Message::Ptr&)> onCommand_dice(TgBot::Bot& bot);

std::function<void(const TgBot::Message::Ptr&)> onCommand_roll(TgBot::Bot& bot, std::mt19937& rng);

std::function<void(const TgBot::Message::Ptr&)> onCommand_or(TgBot::Bot& bot, std::mt19937& rng);
