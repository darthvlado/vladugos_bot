#include "onLatexCommands.h"

#include "common.h"
#include "exec.h"

#include <string>
#include <vector>
#include <filesystem>
#include <chrono>
#include <future>

using namespace std;

function<void(const TgBot::Message::Ptr&)> onCommand_help_latex(TgBot::Bot& bot) {
    return [&bot](const TgBot::Message::Ptr& message) {
        bot.getApi().sendMessage(
                message->chat->id,
                R"(
СПРАВКА ПО LATEX:

https://en.wikibooks.org/wiki/LaTeX/Mathematics
http://mirrors.ibiblio.org/CTAN/macros/latex/contrib/physics/physics.pdf

В преамбуле установлены amsmath, amssymb, physics, tikz и некоторые другие пакеты.
• /latex - содержимое будет помещено в тело документа.
• /latexeq - содержимое будет помещено в {equation}.

Также для удобства добавлены некоторые дополнительные команды:
• \const - константа
• \opgrad - градиент
• \opdiv - дивергенция
• \oprot - ротор
• \oplaplace - оператор Лапласа
• \opdalembert - оператор Даламбера

Добавлены интегралы, более близкие к русской типографской традиции:
• \rint, \riint, \riiint - интегралы
• \roint, \roiint, \roiiint - интегралы по замкнутому контуру
)",
                true
        );
    };
}

void makeLatex(bool isEquation, const TgBot::Message::Ptr& message, TgBot::Bot& bot) {
    vector<string> banned{
            "\\input",
            "\\immediate",
            "\\write18",
            "\\newcommand",
            "\\renewcommand",
            "\\NewDocumentCommand",
            "\\newenvironment"
    };

    for (const auto& i : banned) {
        if (message->text.find(i) != string::npos) {
            string name = message->from->username.empty()
                               ? "anonymous"
                               : message->from->username;
            throw invalid_argument(
                    to_string(message->from->id) + '\t' + name +
                    " ATTEMPTED TO ACCESS THROUGH LATEX");
        }
    }

    ifstream preambule("../src/latex-preambule.txt", ios::in);
    string filename = to_string(time(nullptr)) + "-latex";
    ofstream tex(filename + ".tex", ios::trunc | ios::out);

    auto clear = [&filename]() {
        filesystem::remove(filename + ".jpg");
        filesystem::remove(filename + ".tex");
        filesystem::remove(filename + ".log");
        filesystem::remove(filename + ".aux");
        filesystem::remove(filename + ".pdf");
    };

    string str;
    while (getline(preambule, str)) {
        tex << str << '\n';
    }
    preambule.close();

    if (isEquation) {
        tex << " \\begin{document} \\begin{equation*} " << '\n';
        tex << message->text.erase(0, 8) << '\n';
        tex << " \\end{equation*} \\end{document} " << '\n';
    } else {
        tex << " \\begin{document} \n" << '\n';
        tex << message->text.erase(0, 6) << '\n';
        tex << " \\end{document} " << '\n';
    }
    tex.close();

    future<string> future = async(launch::async, [&filename]() {
        return exec_unsafe(
                "pdflatex -halt-on-error " + filename + ".tex | grep '^!.*' | grep -v 'no output PDF'");
    });

    future_status status = future.wait_for(chrono::seconds(4));
    string result = future.get();

    if (status == future_status::timeout) {
        system("killall pdflatex");
        system("killall convert");
        bot.getApi().sendMessage(
                message->chat->id,
                "Ошибка: превышено время ожидания."
        );
        clear();
        return;
    }

    if (!filesystem::exists(filename + ".pdf")) {
        bot.getApi().sendDocument(
                message->chat->id,
                TgBot::InputFile::fromFile(filename + ".log", "text/plain"),
                "",
                result);
        clear();
        return;
    }

    system(("convert -density 1200 " + filename
            + ".pdf -quality 100 -flatten -sharpen 0x1.0 -trim "  + filename + ".jpg").c_str());

    bot.getApi().sendPhoto(
            message->chat->id,
            TgBot::InputFile::fromFile(filename + ".jpg", "image/jpeg"),
            get_any_name(message) + ": " + message->text
    );

    if (is_bot_admin(message, bot)) {
        bot.getApi().deleteMessage(
                message->chat->id,
                message->messageId
        );
    }
    clear();
}

function<void(const TgBot::Message::Ptr& msg)> onCommand_latexeq(TgBot::Bot& bot) {
    return [&bot](const TgBot::Message::Ptr& message) {
        makeLatex(true, message, bot);
    };
}

function<void(const TgBot::Message::Ptr& msg)> onCommand_latex(TgBot::Bot& bot) {
    return [&bot](const TgBot::Message::Ptr& message) {
        makeLatex(false, message, bot);
    };
}

