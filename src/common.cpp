#include "common.h"

#include <iostream>
#include <sstream>
#include <filesystem>

using namespace std;

void log(const TgBot::Message::Ptr& message, bool enable_log_file, bool is_anon_chat) {
    auto username = message->from->username.empty()
                    ? "anonymous"
                    : message->from->username;
    auto firstName = message->from->firstName.empty()
                     ? "anonymous"
                     : message->from->firstName;
    auto lastName = message->from->lastName.empty()
                    ? "anonymous"
                    : message->from->lastName;
    auto nameFile = is_anon_chat
                    ? "log/anon_chat.txt"
                    : message->chat->type == TgBot::Chat::Type::Private
                      ? "log/" + get_any_name(message) + '_' + to_string(message->chat->id) + ".txt"
                      : "log/" + message->chat->title + '_' + to_string(message->chat->id) + ".txt";

    stringstream ss;
    ss <<
       put_time(gmtime(reinterpret_cast<const long int*>(&message->date)), "%c %Z") << " | " <<
       to_string(message->from->id) + " | " <<
       username + " | " <<
       firstName + " | " <<
       lastName + " | " <<
       '\"' + message->text + '\"' << endl;
    if (!is_anon_chat) {
        cout << ss.str();
    }
    if (enable_log_file) {
        ofstream logFile(nameFile, ios::app | ios::out);
        logFile << ss.str();
        logFile.close();
    }
}

int mod(int divident, int divisor) {
    return (divident % divisor + divisor) % divisor;
}

bool is_admin(const TgBot::Message::Ptr& message, TgBot::Bot& bot) {
    auto status = bot.getApi().getChatMember(message->chat->id, message->from->id)->status;
    return status == "administrator" || status == "creator";
}

bool is_bot_admin(const TgBot::Message::Ptr& message, TgBot::Bot& bot) {
    auto status = bot.getApi().getChatMember(message->chat->id, bot.getApi().getMe()->id)->status;
    return status == "administrator" || status == "creator";
}

bool is_this_bot(const TgBot::Message::Ptr& message, TgBot::Bot& bot) {
    return message->chat->id == bot.getApi().getMe()->id;
}

bool is_reply(const TgBot::Message::Ptr& message) {
    return message->replyToMessage != nullptr;
}

void cut_command_message(const TgBot::Message::Ptr& message) {
    message->text.erase(message->entities[0]->offset, message->entities[0]->length);
}

string get_any_name(const TgBot::Message::Ptr& message) {
    return
            !message->from->username.empty()
            ? message->from->username
            : !message->from->firstName.empty()
              ? message->from->firstName
              : !message->from->lastName.empty()
                ? message->from->lastName
                : "anonimous";
}

string get_mention(const TgBot::Message::Ptr& message, string mention) {
    if (mention.empty()) {
        mention = get_any_name(message);
    }
    return '[' + mention + "](tg://user?id=" + to_string(message->from->id) + ')';
}

size_t get_length(string& str) {
    return wstring_convert<codecvt_utf8 <char32_t>, char32_t>().from_bytes(str).size();
}

void update_database(nlohmann::json& db) {
    try {
        filesystem::copy_file("database.json", "database.json.old", filesystem::copy_options::update_existing);
    } catch (filesystem::filesystem_error& e) {
        cout << e.what() << endl;
    }

    try {
        ofstream ofs("database.json", ios::out | ios::trunc);
        ofs << setw(4) << db << endl;
        ofs.close();
    } catch (fstream::failure& e) {
        cout << e.what() << endl;
        terminate();
    }

    cout << "database succesfulyl updated" << endl;
}