#pragma once

#include "json.hpp"
#include <tgbot/Bot.h>
#include <tgbot/types/Message.h>
#include <functional>

std::function<void(const TgBot::Message::Ptr&)> onCommand_set_welcome(TgBot::Bot& bot, nlohmann::json& db);

std::function<void(const TgBot::Message::Ptr&)> onCommand_switch_welcome(TgBot::Bot& bot, nlohmann::json& db);
