#pragma once

#include <tgbot/Bot.h>
#include <tgbot/types/Message.h>
#include <functional>

std::function<void(const TgBot::Message::Ptr&)> onCommand_help_latex(TgBot::Bot& bot);

std::function<void(const TgBot::Message::Ptr&)> onCommand_latexeq(TgBot::Bot& bot);

std::function<void(const TgBot::Message::Ptr&)> onCommand_latex(TgBot::Bot& bot);
