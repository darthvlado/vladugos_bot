#pragma once

#include <sys/wait.h>
#include <string>
#include <vector>
#include <array>

std::string exec(const std::vector<std::string>& args, bool inc_stderr = false, volatile pid_t* pd = nullptr);

std::string exec_unsafe(const std::string& command);
